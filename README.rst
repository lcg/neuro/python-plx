lcg-neuro-plx -- Parsing of PLX neurophysiological data files in Python
=======================================================================

+-----------------------------------------------------------------------------------------------------------+
| |badge-python-version| |badge-version|                                                                    | 
+-----------------------------------------------------------------------------------------------------------+
| |badge-license| |badge-python-style|                                                                      |
+-----------------------------------------------------------------------------------------------------------+
| |badge-pipeline| |badge-security| |badge-codecov|                                                         |
+-----------------------------------------------------------------------------------------------------------+
| `Documentation <https://lcg.gitlab.io/neuro/python-plx>`__                                                |
+-----------------------------------------------------------------------------------------------------------+
| `Issue tracker <https://gitlab.com/lcg/neuro/python-plx/issues>`__                                        |
+-----------------------------------------------------------------------------------------------------------+
| `Repository contents <https://gitlab.com/lcg/neuro/python-plx/blob/master/MANIFEST.rst>`__                |
+-----------------------------------------------------------------------------------------------------------+
| `History of changes <https://gitlab.com/lcg/neuro/python-plx/blob/master/CHANGELOG.rst>`__                |
+-----------------------------------------------------------------------------------------------------------+
| `Contribution/development guide <https://gitlab.com/lcg/neuro/python-plx/blob/master/CONTRIBUTING.rst>`__ |
+-----------------------------------------------------------------------------------------------------------+
| `License <https://gitlab.com/lcg/neuro/python-plx/blob/master/LICENSE.txt>`__                             |
+-----------------------------------------------------------------------------------------------------------+


Installation
------------

.. code:: bash

    pip install lcg-neuro-plx

You'll need g++ installed in order to compile the bundled extensions.
Up to the current version, only source distributions are available on PyPI_.

Usage
-----

See the quick reference guide at https://lcg.gitlab.io/neuro/python-plx/quickref.html.

--------------

- Powered by `GitLab CI <https://docs.gitlab.com/ee/ci>`__
- Created by `Pedro Asad <pasad@lcg.ufrj.br> <mailto:pasad@lcg.ufrj.br>`__
  using `cookiecutter <http://cookiecutter.readthedocs.io/>`__
  and `@pedroasad.com/templates/python/python/app-1.0.0 <https://gitlab.com/pedroasad.com/templates/python/python-app/tags/1.0.0>`__

 .. _PyPI: https://pypi.org

.. |badge-python-version| image:: https://img.shields.io/badge/Python-%E2%89%A53.6-blue.svg
   :target: https://docs.python.org/3.6

.. |badge-version| image:: https://img.shields.io/badge/version-0.3.1%20-orange.svg
   :target: https://test.pypi.org/project/lcg-neuro-plx/0.3.1/

.. |badge-license| image:: https://img.shields.io/badge/license-MIT-blue.svg
   :target: https://opensource.org/licenses/MIT

.. |badge-python-style| image:: https://img.shields.io/badge/code%20style-Black-black.svg
   :target: https://pypi.org/project/black/

.. |badge-pipeline| image:: https://gitlab.com/lcg/neuro/python-plx/badges/master/pipeline.svg
   :target: https://gitlab.com/lcg/neuro/python-plx

.. |badge-security| image:: https://img.shields.io/badge/security-Check%20here!-yellow.svg
   :target: https://gitlab.com/lcg/neuro/python-plx/security

.. |badge-codecov| image:: https://codecov.io/gl/lcg:neuro/python-plx/branch/master/graph/badge.svg
   :target: https://codecov.io/gl/lcg:neuro/python-plx

