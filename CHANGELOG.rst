=========
Changelog
=========

All notable changes to this project will be documented in this file.
The format is *based* on `Keep a Changelog`_ (adapted to *reStructuredText*) and this project adheres to `Semantic Versioning`_.

.. contents:: Table of contents
    :local:
    :depth: 1

----

Unreleased_
===========
0.3.1_ -- 2019-08-07
====================
Changed
-------
API
~~~
* Error message thrown by :mod:`_plx` C-extension on bad data-blocks now informs the file offset of the data-block.

0.3.0_ -- 2019-08-06
====================
Changed
-------
API
~~~

* Default export dir for spike data changed from ``slow_channels`` to ``slow_data``.
* Default export dir for spike data changed from ``spike_channels`` to ``spike_data``.
* Exported test data directories under :file:`tests/data/229` renamed accordingly.

Removed
-------

* Useless test code in :file:`

0.2.1_ -- 2019-08-06
====================
Bugs
----
Command-line interface
~~~~~~~~~~~~~~~~~~~~~~

* Fixed import error on command line module :mod:`plx.__main__`.

0.2.0_ -- 2019-07-18
====================
Added
-----
API
~~~

* Module :mod:`plx`

  * Attribute :data:`plx.units`

Changed
-------
API
~~~

* The library-wide :class:`pint.UnitRegistry` instance is now

  * Transparently accessed by a proxy, and
  * Updated through :meth:`plx.units.set_registry`.

Removed
-------

API
~~~

* Module :mod:`plx.config`

Files/directories
~~~~~~~~~~~~~~~~~

* ``src/plx/config.py``

0.1.0_ -- 2019-07-17
====================
Added
-----
Command-line interface
~~~~~~~~~~~~~~~~~~~~~~

* :mod:`plx.__main__`

  * Command :program:`plx.__main__:main`

    * Sub-command :program:`plx.__main__:main_export`

API
~~~

* Module :mod:`_plx`

  * Function :func:`_plx.export_channels`

* Module :mod:`plx`

  * Class :class:`plx.AbstractPlxFile`
  * Class :class:`plx.ExportedPlxFile`
  * Class :class:`plx.PlxFile`
  * Class :class:`plx.SlowChannel`
  * Class :class:`plx.SpikeChannel`

  * Module :mod:`plx.config`

    * Attribute :data:`plx.config.units`

  * Module :mod:`plx.export`

    * Attribute :data:`plx.export.exported_slow_dtype`
    * Class :class:`plx.export.ExportedEvent`
    * Function :func:`plx.export.ExportedSpike`
    * Function :func:`plx.export.make_dirs`

    * Module :mod:`plx.export.paths`

      * Function :func:`plx.export.paths.counts`
      * Function :func:`plx.export.paths.event_data`
      * Function :func:`plx.export.paths.event_headers`
      * Function :func:`plx.export.paths.header`
      * Function :func:`plx.export.paths.slow_channel`
      * Function :func:`plx.export.paths.slow_channels_dir`
      * Function :func:`plx.export.paths.slow_headers`
      * Function :func:`plx.export.paths.spike_channel`
      * Function :func:`plx.export.paths.spike_channels_dir`
      * Function :func:`plx.export.paths.spike_headers`

  * Module :mod:`plx.parsing`

    * Class :class:`plx.parsing.CountSpec`
    * Class :class:`plx.parsing.Counts`
    * Class :class:`plx.parsing.DataBlockHeader`
    * Class :class:`plx.parsing.EventHeader`
    * Class :class:`plx.parsing.Header`
    * Class :class:`plx.parsing.SlowHeader`
    * Class :class:`plx.parsing.SpikeHeader`

  * Module :mod:`plx.schemas`

    * Class :class:`plx.schemas.Counts`
    * Class :class:`plx.schemas.Event`
    * Class :class:`plx.schemas.EventData`
    * Class :class:`plx.schemas.EventHeader`
    * Class :class:`plx.schemas.Header`
    * Class :class:`plx.schemas.PlxFile`
    * Class :class:`plx.schemas.SlowChannel`
    * Class :class:`plx.schemas.SlowHeader`
    * Class :class:`plx.schemas.SpikeChannel`
    * Class :class:`plx.schemas.SpikeData`
    * Class :class:`plx.schemas.SpikeHeader`

  * Module :mod:`recparse`

    * Class :class:`recparse.FieldSpec`
    * Class :class:`recparse.Record`
    * Class :class:`recparse.RecordArray`
    * Class :class:`recparse.RecordMeta`

Files/directories
~~~~~~~~~~~~~~~~~

* `.bumpversion.cfg <https://gitlab.com/lcg/neuro/python-plx/blob/0.1.0/.bumpversion.cfg>`_ --- Configuration file for the bumpversion_ version-tagging package.
* `.coveragerc <https://gitlab.com/lcg/neuro/python-plx/blob/0.1.0/.coveragerc>`_ --- Configuration file for the Coverage_ reporting tool.
* `.gitignore <https://gitlab.com/lcg/neuro/python-plx/blob/0.1.0/.gitignore>`_ --- List of files and directories paths/patterns `ignored by Git <gitignore_>`_.
* `.gitlab-ci.yml <https://gitlab.com/lcg/neuro/python-plx/blob/0.1.0/.gitlab-ci.yml>`_ --- Continuous integration/deploy configuration (`GitLab CI`_), configured for:

  * Running a pytest_-based test suite and reporting results with Codecov_ at https://codecov.io/gl/lcg:neuro/python-plx,
  * Building the Sphinx_-based documentation and deploying it via `Gitlab Pages`_ to https://lcg.gitlab.io/neuro/python-plx, and
  * Uploading successfully built, tagged distributions to TestPyPI_ (defaults to https://test.pypi.org/project/lcg-neuro-plx).

* `.pre-commit-config.yaml <https://gitlab.com/lcg/neuro/python-plx/blob/0.1.0/.pre-commit-config.yaml>`_ --- Configuration file for the pre-commit_ package, which aids in applying useful `Git Hooks`_ in team workflows. Includes:

  * Automatic code styling with Black_.

* `CHANGELOG.rst <https://gitlab.com/lcg/neuro/python-plx/blob/0.1.0/CHANGELOG.rst>`_ --- This very history file, which follows the `Keep a Changelog`_ standard.
* `CONTRIBUTING.rst <https://gitlab.com/lcg/neuro/python-plx/blob/0.1.0/CONTRIBUTING.rst>`_ --- General orientations for developers.
* `LICENSE.txt <https://gitlab.com/lcg/neuro/python-plx/blob/0.1.0/LICENSE.txt>`_ --- Copy of the `MIT License`_.
* `MANIFEST.rst <https://gitlab.com/lcg/neuro/python-plx/blob/0.1.0/MANIFEST.rst>`_ --- Repository's manifest.
* `README.rst <https://gitlab.com/lcg/neuro/python-plx/blob/0.1.0/README.rst>`_ --- Repository front-page.
* `docs/ <https://gitlab.com/lcg/neuro/python-plx/blob/0.1.0/docs>`_ --- Sphinx_-based documentation setup, which includes:

  * `conf.py <https://gitlab.com/lcg/neuro/python-plx/blob/0.1.0/docs/conf.py>`_ --- Sphinx_ configuration file,
  * `docs/index.rst <https://gitlab.com/lcg/neuro/python-plx/blob/0.1.0/docs/index.rst>`_ --- Documentation master file, and
  * `Makefile <https://gitlab.com/lcg/neuro/python-plx/blob/0.1.0/docs/Makefile>`_ for building the docs easily.

* `poetry.lock <https://gitlab.com/lcg/neuro/python-plx/blob/0.1.0/poetry.lock>`_ --- Poetry_'s resolved dependency file. Whereas `pyproject.toml <https://gitlab.com/lcg/neuro/python-plx/blob/0.1.0/pyproject.toml>`_ specifies `as abstract as possible <setup-vs-requirements_>`__ dependencies, this file
* `pyproject.toml <https://gitlab.com/lcg/neuro/python-plx/blob/0.1.0/pyproject.toml>`_ --- PEP-517_-compliant packaging metadata, configured with the Poetry_ system. Includes, among other information: package qualifiers, version, author, and all of its dependencies. This file replaces the classic setup.py_ file found in *classical* Python packaging.
* `src/plx <https://gitlab.com/lcg/neuro/python-plx/blob/0.1.0/plx>`_ --- Base directory of the example Python package distributed by this repository.
* `tests <https://gitlab.com/lcg/neuro/python-plx/blob/0.1.0/tests>`_ --- pytest_-powered test-suite.

.. _LICENSE: https://gitlab.com/lcg/neuro/python-plx/blob/master/LICENSE.txt
.. _MANIFEST: https://gitlab.com/lcg/neuro/python-plx/blob/master/MANIFEST.md
.. _README: https://gitlab.com/lcg/neuro/python-plx/blob/master/README.md
.. _Unreleased: https://gitlab.com/lcg/neuro/python-plx/compare?from=release&to=master
.. _0.3.1: https://gitlab.com/lcg/neuro/python-plx/compare?from=0.3.0&to=0.3.1
.. _0.3.0: https://gitlab.com/lcg/neuro/python-plx/compare?from=0.2.1&to=0.3.0
.. _0.2.1: https://gitlab.com/lcg/neuro/python-plx/compare?from=0.2.0&to=0.2.1
.. _0.2.0: https://gitlab.com/lcg/neuro/python-plx/compare?from=0.1.0&to=0.2.0
.. _0.1.0: https://gitlab.com/lcg/neuro/python-plx/tags/0.1.0

.. _Black: https://pypi.org/project/black/
.. _Codecov: https://codecov.io
.. _Coverage: https://coverage.readthedocs.io
.. _Git hooks: https://git-scm.com/book/en/v2/Customizing-Git-Git-Hooks
.. _Gitlab CI: https://docs.gitlab.com/ee/ci
.. _Gitlab Pages: https://docs.gitlab.com/ee/user/project/pages/
.. _Keep a Changelog: https://keepachangelog.com/en/1.0.0/
.. _MIT License: https://opensource.org/licenses/MIT
.. _PEP-517: https://www.python.org/dev/peps/pep-0517/
.. _Poetry: https://github.com/sdispater/poetry
.. _Poetry: https://github.com/sdispater/poetry
.. _PyPI: https://pypi.org
.. _Semantic Versioning: https://semver.org/spec/v2.0.0.html
.. _Sphinx: https://www.sphinx-doc.org/en/master/index.html
.. _TestPyPI: https://test.pypi.org
.. _bumpversion: https://github.com/peritus/bumpversion
.. _gitignore: https://git-scm.com/docs/gitignore
.. _open-source: https://opensource.org/
.. _post-setup-vs-requirements: https://caremad.io/posts/2013/07/setup-vs-requirement/
.. _pre-commit: https://pre-commit.com/
.. _pytest: https://pytest.org/
.. _repository-codecov: https://codecov.io/gl/lcg:neuro/python-plx
.. _setup-vs-requirements: https://caremad.io/posts/2013/07/setup-vs-requirement/
.. _setup.py: https://docs.python.org/3.6/distutils/setupscript.html
