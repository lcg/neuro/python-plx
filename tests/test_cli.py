import click.testing
import numpy
import pytest

import plx.__main__


class Test_commands:
    """Commands with form :code:`python -m plx [...]"""

    @pytest.fixture(scope="class")
    def plx_instance(self, plx_path) -> plx.ExportedPlxFile:
        """An instance of :class:`plx.ExportedPlxFile` opened on one of the *PLX* test files provided."""
        return plx.ExportedPlxFile(plx_path.parent)

    def test_COMMAND_export_OPT_outdir_TMPDIR(
        self, request, plx_instance, plx_path, tmp_path_factory
    ):
        """This command splits a given PLX file into multiple binary files, in a reordering of the original content.

        Given
        -----
        * One of the test PLX files from :data:`tested_files` and its corresponding split parts
        * A copy of this PLX file in a temporary directory
        1. :code:`python -m plx export <copied_plx_file_path>`

        Then
        ----
        * Return code is 0
        * Main header section is correctly exported to a binary file
        * Counts section is correctly exported to a binary file
        * Event headers section is correctly exported to a binary file
        * Slow channel headers section is correctly exported to a binary file
        * Spike channel headers section is correctly exported to a binary file
        * All event data blocks are correctly exported to a condensed binary file
        * All slow channel data blocks are correctly exported to condensed binary files, one for each channel
        * All spike channel data blocks are correctly exported to condensed binary files, one for each channel
        """

        outdir = tmp_path_factory.mktemp(request.node.name)

        cli_runner = click.testing.CliRunner()
        cli_params = f"export {plx_path} --outdir {outdir!s}".split()
        cli_result = cli_runner.invoke(plx.__main__.main, cli_params)

        assert cli_result.exit_code == 0

        exported = plx.ExportedPlxFile(outdir)

        assert exported.header == plx_instance.header
        assert exported.counts == plx_instance.counts

        assert exported.event_headers == plx_instance.event_headers
        assert exported.slow_headers == plx_instance.slow_headers
        assert exported.spike_headers == plx_instance.spike_headers

        assert numpy.all(exported.event_data() == plx_instance.event_data())

        for channel in range(1, len(plx_instance.slow_headers) + 1):
            expected_slow_channels = plx_instance.slow_channel(channel).data
            actual_slow_channels = exported.slow_channel(channel).data
            assert numpy.all(actual_slow_channels == expected_slow_channels)

        for channel in range(1, len(plx_instance.spike_headers)):
            expected_spike_channels = plx_instance.spike_channel(channel).data
            actual_spike_channels = exported.spike_channel(channel).data
            assert numpy.all(actual_spike_channels == expected_spike_channels)
