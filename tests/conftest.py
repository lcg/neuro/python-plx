import attr
import pathlib
import numpy
import os
import plx
import pytest

from tests import lib
from typing import *
from pathlib import Path
from plx import ExportedPlxFile

_tests_root_dir = pathlib.Path(__file__).parent
_tests_data_dir = _tests_root_dir / "data"
_tests_plx_paths = {
    _plx_path.parent.stem: _plx_path
    for _plx_path in _tests_data_dir.rglob("*/file.plx")
    # Only keep files smaller than 100 MB to prevent slowing down the pipeline too much.
    if os.stat(_plx_path).st_size < 100e6
}


@attr.s(auto_attribs=True, frozen=True)
class DatasetRecordingFixture:
    plx_path: Path = attr.ib()

    @property
    def id(self):
        return int(self.plx_dir.stem)

    @property
    def plx_dir(self) -> Path:
        return self.plx_path.parent

    @property
    def plx_file(self) -> ExportedPlxFile:
        return ExportedPlxFile(base_dir=self.plx_dir)

    def spike_channel(self, channel):
        spike_headers = numpy.fromfile(
            str(self.plx_dir / "spike_headers.bin"), dtype=plx.parsing.SpikeHeader.dtype
        )
        spike_dtype = plx.export.ExportedSpike(self.plx_file).dtype
        spike_path = self.plx_dir / "spike_data" / f"{channel}.bin"
        if spike_path.exists():
            spike_data = numpy.fromfile(str(spike_path), dtype=spike_dtype)
        else:
            spike_data = numpy.array([], dtype=spike_dtype)
        spike_header = spike_headers[channel - 1]
        return plx.SpikeChannel(header=spike_header, data=spike_data)


@pytest.fixture(
    scope="session", ids=_tests_plx_paths.keys(), params=_tests_plx_paths.values()
)
def plx_path(request) -> pathlib.Path:
    """A fixture :class:`pathlib.Path` object pointing to one of the *PLX* test files included for testing."""
    return request.param


@pytest.fixture(
    scope="session", ids=_tests_plx_paths.keys(), params=_tests_plx_paths.values()
)
def dataset_recording(request) -> DatasetRecordingFixture:
    return DatasetRecordingFixture(plx_path=request.param)


@pytest.fixture(scope="session")
def tests_data_dir():
    """Points to the ``data/tests`` directory, relative to the repository's root.

    Returns
    -------
    pathlib.Path
    """
    return _tests_data_dir
