# -*-coding: utf-8 -*-
#
# projectv2 - Visual investigations of electrical recordings on the primate secondary visual cortex
# Copyright (C) 2019 Laboratório de Computação Gráfica
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# If you have any questions or suggestions regarding this software, visit
# <http://gitlab.com/lcg/v2/projectv2>.

import pytest
import plx.export.paths as module


@pytest.fixture(scope="session")
def plx_base_dir(plx_path):
    return plx_path.parent


def test_counts(plx_base_dir):
    assert module.counts(plx_base_dir) == plx_base_dir / "counts.bin"


def test_event_data(plx_base_dir):
    assert module.event_data(plx_base_dir) == plx_base_dir / "event_data.bin"


def test_event_headers(plx_base_dir):
    assert module.event_headers(plx_base_dir) == plx_base_dir / "event_headers.bin"


def test_header(plx_base_dir):
    assert module.header(plx_base_dir) == plx_base_dir / "header.bin"


def test_slow_headers(plx_base_dir):
    assert module.slow_headers(plx_base_dir) == plx_base_dir / "slow_headers.bin"


def test_spike_headers(plx_base_dir):
    assert module.spike_headers(plx_base_dir) == plx_base_dir / "spike_headers.bin"


@pytest.mark.parametrize("channel", [1, 10])
def test_slow_channels(plx_base_dir, channel):
    assert (
        module.slow_channel(plx_base_dir, channel)
        == plx_base_dir / "slow_data" / f"{channel}.bin"
    )


@pytest.mark.parametrize("channel", [1, 10])
def test_spike_channels(plx_base_dir, channel):
    assert (
        module.spike_channel(plx_base_dir, channel)
        == plx_base_dir / "spike_data" / f"{channel}.bin"
    )


def test_slow_channels_dir(plx_base_dir):
    assert module.slow_channels_dir(plx_base_dir) == plx_base_dir / "slow_data"


def test_spike_channels_dir(plx_base_dir):
    assert module.spike_channels_dir(plx_base_dir) == plx_base_dir / "spike_data"
