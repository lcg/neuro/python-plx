import json
import numpy


def array_from_json(fname) -> numpy.ndarray:
    """Test helper: loads a :class:`numpy.ndarray` from a *JSON* file.
    """
    with open(fname) as file:
        data = json.load(file)
    if len(data) == 0:
        return numpy.array([])
    else:
        dtype = [(key, type(val)) for key, val in data[0].items()]
        array = numpy.empty(len(data), dtype=dtype)
        for i, record in enumerate(data):
            for key, val in record.items():
                array[i][key] = val
        return array


def load_json(fname, safe=False):
    try:
        file = open(fname)
        obj = json.load(file)
        file.close()
        return obj
    except IOError:
        if safe:
            return {}
        else:
            raise
