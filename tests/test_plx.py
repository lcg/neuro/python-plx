import json
import numpy
import os
import pathlib
import pytest
import plx
import plx.export as module


class Test_AbstractPlxFile:
    """Test cases common to the :class:`plx.ExportedPlxfile` and :class:`plx.PlxFile` classes.
    """

    @pytest.fixture(scope="class", params=[plx.ExportedPlxFile, plx.PlxFile])
    def plx_class(self, request) -> type:
        """Either of the concrete subclasses from :class:`plx.AbstractPlFile`."""
        return request.param

    @pytest.fixture(scope="class")
    def plx_instance(
        self, plx_class, plx_path, plx_export_impl, tmp_path_factory
    ) -> plx.AbstractPlxFile:
        """An instance of a PLX parsing class opened on one of the *PLX* test files provided."""
        export_dir = pathlib.Path(tmp_path_factory.getbasetemp()).joinpath(
            plx_class.__name__, plx_path.parent.stem, plx_export_impl
        )
        export_dir.mkdir(parents=True)
        # export_dir.mkdir()
        # export_dir = tmp_path_factory.mktemp(request.node.name)

        use_c_ext = plx_export_impl == "c_ext"
        plx_file = plx.PlxFile(
            path=plx_path, export_base_dir=export_dir, use_c_ext=use_c_ext
        )

        if issubclass(plx_class, plx.PlxFile):
            plx_file.export_channels()
            return plx_file
        elif issubclass(plx_class, plx.ExportedPlxFile):
            return plx_file.export_file()

    @pytest.fixture(scope="class", params=["python", "c_ext"])
    def plx_export_impl(self, request) -> str:
        """Alternates between the C-extension (:mod:`v2._plx`) and the legacy Python implementation of *PLX* channel
        data exporting (:func:`plx.export.export_channel`)."""
        return request.param

    def test_method__event_data(self, plx_instance, plx_path):
        """Evaluate the ``event_data`` method.

        Given
        -----
        * An :class:`plx.AbstractPlxFile` instance opened on a test file (fixture)
        * The corresponding event data of the *PLX* file, exported and read as a Numpy array

        Then
        ----
        * The result of the ``event_data`` is array-identical to the exported version
        """

        expected_event_data = numpy.fromfile(
            str(plx_path.parent / "event_data.bin"), dtype=module.ExportedEvent.dtype
        )

        assert numpy.all(plx_instance.event_data() == expected_event_data)

    def test_method__slow_channel(self, plx_instance, plx_path):
        """Evaluate the ``slow_channel`` method.

        Given
        -----
        * An :class:`plx.AbstractPlxFile` instance opened on a test file (fixture)
        * The corresponding slow waveform channel data of the *PLX* file, exported and read as a Numpy array for each
          channel

        Then
        ----
        * For each slow waveform channel file, the corresponding item of the ``slow_channel`` method is array-identical to
          this file
        """

        channel_numbers = set()

        for channel_file_path in (plx_path.parent / "slow_channels").glob("*.bin"):
            channel_number = int(channel_file_path.stem)
            channel_numbers.add(channel_number)

            expected_channel_array = numpy.fromfile(
                str(channel_file_path), dtype=module.exported_slow_dtype
            )
            actual_channel_array = numpy.frombuffer(
                plx_instance.slow_channel(channel_number).data,
                dtype=module.exported_slow_dtype,
            )

            assert numpy.all(actual_channel_array == expected_channel_array)

    def test_method__spike_channel(self, plx_instance, plx_path):
        """Evaluate the ``spike_channel`` method.

        Given
        -----
        * An :class:`plx.AbstractPlxFile` instance opened on a test file (fixture).
        * The corresponding sorted spike waveform channel data of the *PLX* file, exported and read as a Numpy array for
          each channel.

        Then
        ----
        * For each sorted spike waveform channel file, the corresponding item of the ``spike_channel`` method is
          array-identical to this file.
        """

        channel_numbers = set()

        for channel_file_path in (plx_path.parent / "spike_channels").glob("*.bin"):
            channel_number = int(channel_file_path.stem)
            channel_numbers.add(channel_number)

            expected_channel_array = numpy.fromfile(
                str(channel_file_path), dtype=module.ExportedSpike(plx_instance).dtype
            )
            actual_channel_array = plx_instance.spike_channel(channel_number).data

            assert numpy.all(actual_channel_array == expected_channel_array)

    def test_property__counts(self, plx_instance, plx_path):
        """Evaluate the fields and binary content of the ``counts`` property.

        Given
        -----
        * An :class:`plx.AbstractPlxFile` instance opened on a test file (fixture)
        * The corresponding *JSON*-exported counts section of the *PLX* file
        * The corresponding binary-exported counts section of the *PLX* file

        Then
        ----
        * For each key, value pair in the *JSON*-exported header section, there is a matching property for the
          ``counts`` instance property that is Numpy-identical
        * The bytes of the ``header`` property equal the binary-exported header section

        TODO: This test case could be conceptually simpler (more similar to the one for the ``header``) property had it
              not been for the limitations of the :mod:`recparse` module, that creates flat arrays in the case of the
              ``TSCounts``, ``WFCounts``, and ``EVCounts`` field descriptors, forcing us to use custom property adapters
              for actually accessing them as the ``timestamps``, ``waveforms``, and ``events`` Numpy arrays.
        """
        with open(plx_path.parent / "counts.json") as json_file:
            json_counts = json.load(json_file)

        with open(plx_path.parent / "counts.bin", "rb") as bin_file:
            bin_counts = bin_file.read()

        for key in json_counts.keys():
            assert numpy.all(getattr(plx_instance.counts, key) == json_counts[key])

        assert bytes(plx_instance.counts) == bin_counts

    def test_property__event_headers(self, plx_instance, plx_path):
        """Evaluate the fields and binary content of the items from in the ``event_headers`` property.

        Given
        -----
        * An :class:`plx.AbstractPlxFile` instance opened on a test file (fixture)
        * The corresponding *JSON*-exported event channels header section of the *PLX* file
        * The corresponding binary-exported event channels header section of the *PLX* file

        Then
        ----
        * The ``event_headers`` property is a sequence, equal to the *JSON*-exported version
        * The bytes of the ``event_headers`` property equal the binary-exported version
        """

        with open(plx_path.parent / "event_headers.json") as json_file:
            json_headers = json.load(json_file)

        assert len(plx_instance.event_headers) == len(json_headers)

        for i in range(len(json_headers)):
            dict(plx_instance.event_headers[i]) == json_headers[i]

    def test_property__header(self, plx_instance, plx_path):
        """Evaluate the fields and binary content of the ``header`` property.

        Given
        -----
        * An :class:`plx.AbstractPlxFile` instance opened on a test file (fixture)
        * The corresponding *JSON*-exported header section of the *PLX* file
        * The corresponding binary-exported header section of the *PLX* file

        Then
        ----
        * The fields of the ``header`` property equal the *JSON*-exported header section
        * The bytes of the ``header`` property equal the binary-exported header section
        """
        with open(plx_path.parent / "header.json") as json_file:
            json_header = json.load(json_file)

        with open(plx_path.parent / "header.bin", "rb") as bin_file:
            bin_header = bin_file.read()

        assert dict(plx_instance.header) == json_header
        assert bytes(plx_instance.header) == bin_header

    def test_property__slow_headers(self, plx_instance, plx_path):
        """Evaluate the fields and binary content of the items from in the ``slow_headers`` property.

        Given
        -----
        * An :class:`plx.AbstractPlxFile` instance opened on a test file (fixture)
        * The corresponding *JSON*-exported slow channels header section of the *PLX* file
        * The corresponding binary-exported slow channels header section of the *PLX* file

        Then
        ----
        * The ``slow_headers`` property is a sequence, equal to the *JSON*-exported version
        * The bytes of the ``slow_headers`` property equal the binary-exported version
        """

        with open(plx_path.parent / "slow_headers.json") as json_file:
            json_headers = json.load(json_file)

        assert len(plx_instance.slow_headers) == len(json_headers)

        for i in range(len(json_headers)):
            dict(plx_instance.slow_headers[i]) == json_headers[i]

    def test_property__spike_headers(self, plx_instance, plx_path):
        """Evaluate the fields and binary content of the items from in the ``spike_headers`` property.

        Given
        -----
        * An :class:`plx.AbstractPlxFile` instance opened on a test file (fixture)
        * The corresponding *JSON*-exported spike channels header section of the *PLX* file
        * The corresponding binary-exported spike channels header section of the *PLX* file

        Then
        ----
        * The ``spike_headers`` property is a sequence, equal to the *JSON*-exported version
        * The bytes of the ``spike_headers`` property equal the binary-exported version
        """

        with open(plx_path.parent / "spike_headers.json") as json_file:
            json_headers = json.load(json_file)

        assert len(plx_instance.spike_headers) == len(json_headers)

        for i in range(len(json_headers)):
            dict(plx_instance.spike_headers[i]) == json_headers[i]

    def test_scenario__concatenate_pre_data_bytes(self, plx_instance, plx_path):
        """Concatenated bytes of pre-data sections should be identical to original test file.
        """
        actual_pre_data_bytes = (
            bytes(plx_instance.header)
            + bytes(plx_instance.counts)
            + bytes(plx_instance.spike_headers)
            + bytes(plx_instance.event_headers)
            + bytes(plx_instance.slow_headers)
        )

        with open(plx_path, "rb") as plx_file:
            expected_pre_data_bytes = plx_file.read(len(actual_pre_data_bytes))

        assert actual_pre_data_bytes == expected_pre_data_bytes


class Test_PlxFile:
    """Tests for the :class:`plx.Plxfile` class.
    """

    @pytest.fixture(scope="class")
    def plx_instance(self, plx_path) -> plx.PlxFile:
        """An instance of :class:`plx.ExportedPlxFile` opened on one of the *PLX* test files provided."""
        return plx.PlxFile(plx_path)

    def test_property__size(self, plx_instance, plx_path):
        assert plx_instance.size == os.stat(plx_path).st_size
