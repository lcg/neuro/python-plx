import collections
import pytest
import struct
import recparse


class Test_RecordArray:
    @pytest.fixture(scope="class")
    def record_type(self):
        class RecordType(recparse.Record):
            FIELDS = [
                recparse.FieldSpec("a", "i"),
                recparse.FieldSpec("b", "f"),
                recparse.FieldSpec("c", "H"),
            ]

        return RecordType

    @pytest.fixture(scope="class")
    def record_instances(self, record_type):
        return [
            record_type.from_mapping({"a": 400, "b": -64.25, "c": 2 ** 15}),
            record_type.from_mapping({"a": -50, "b": -720.5, "c": 20915}),
            record_type.from_mapping({"a": 0xA, "b": 478976, "c": 36415}),
        ]

    def test__constructor__from_iterable(self, record_type, record_instances):
        record_array = recparse.RecordArray.from_iterable(
            record_instances, record_type=record_type
        )

        assert len(record_array) == len(record_instances)

        for i in range(len(record_instances)):
            assert record_array[i] == record_instances[i]

        assert bytes(record_array) == b"".join(bytes(rec) for rec in record_instances)

    def test__constructor__from_stream(self, record_type, record_instances, tmpdir):
        path = str(tmpdir.join("record_array.bin"))
        with open(path, "wb") as file:
            for record in record_instances:
                file.write(bytes(record))

        with open(path, "rb") as file:
            record_array = recparse.RecordArray.from_stream(
                file, record_type=record_type
            )

        assert len(record_array) == len(record_instances)

        for i in range(len(record_instances)):
            assert record_array[i] == record_instances[i]

        assert bytes(record_array) == b"".join(bytes(rec) for rec in record_instances)

    def test__operation__delitem(self, record_type, record_instances):
        record_array = recparse.RecordArray.from_iterable(
            record_instances, record_type=record_type
        )

        del record_array[1]

        assert record_array[0] == record_instances[0]
        assert record_array[1] == record_instances[2]

    def test__operation__equality(self, record_type, record_instances):
        record_array1 = recparse.RecordArray.from_iterable(
            record_instances, record_type=record_type
        )
        record_array2 = recparse.RecordArray.from_iterable(
            record_instances, record_type=record_type
        )

        assert record_array1 == record_array2

    def test__operation__setitem(self, record_type, record_instances):
        record_array = recparse.RecordArray.from_iterable(
            record_instances, record_type=record_type
        )

        record_array[0] = record_array[2]

        assert record_array[0] == record_array[2]


class Test__Record__with_just_fields:
    @pytest.fixture(scope="class")
    def record_type(self):
        class RecordType(recparse.Record):
            FIELDS = [
                recparse.FieldSpec("a", "i"),
                recparse.FieldSpec("b", "f"),
                recparse.FieldSpec("c", "H"),
            ]

        return RecordType

    def test__class_method__fields(self, record_type):
        fields = collections.OrderedDict(
            [
                ("a", recparse.FieldSpec("a", "i", offset=0)),
                ("b", recparse.FieldSpec("b", "f", offset=4)),
                ("c", recparse.FieldSpec("c", "H", offset=8)),
            ]
        )

        assert record_type.fields() == fields

    def test__class_method__padding(self, record_type):
        assert record_type.padding() == 0

    def test__class_method__struct_format(self, record_type):
        assert record_type.struct_format() == "ifH"

    def test__class_method__total_size(self, record_type):
        assert record_type.total_size() == 10

    def test__class_method__used_size(self, record_type):
        assert record_type.used_size() == 10

    def test__constructor__empty(self, record_type, tmpdir):
        a, b, c = attributes = struct.unpack("ifH", bytes(10))

        record = record_type.empty()

        assert record["a"] == a
        assert record["b"] == b
        assert record["c"] == c

        assert len(record) == 3
        assert bytes(record) == bytes(10)

        assert list(record.keys()) == "a b c".split()
        assert tuple(record.values()) == attributes
        assert list(record.items()) == list(zip("a b c".split(), attributes))

    @pytest.mark.parametrize("bytes_type", [bytes, bytearray])
    def test__constructor__from_bytes(self, record_type, bytes_type):
        a, b, c = attributes = (-2 ** 31, 1234.5, 2 ** 15 - 1)

        source_bytes = struct.pack("ifH", *attributes)
        record = record_type.from_bytes(bytes_type(source_bytes))

        assert record["a"] == a
        assert record["b"] == b
        assert record["c"] == c

        assert len(record) == 3
        assert bytes(record) == source_bytes

        assert list(record.keys()) == "a b c".split()
        assert tuple(record.values()) == attributes
        assert list(record.items()) == list(zip("a b c".split(), attributes))

    def test__constructor__from_iterable(self, record_type):
        a, b, c = attributes = (-2 ** 31, 1234.5, 2 ** 15 - 1)

        source_bytes = struct.pack("ifH", *attributes)
        record = record_type.from_iterable(attributes)

        assert record["a"] == a
        assert record["b"] == b
        assert record["c"] == c

        assert len(record) == 3
        assert bytes(record) == source_bytes

        assert list(record.keys()) == "a b c".split()
        assert tuple(record.values()) == attributes
        assert list(record.items()) == list(zip("a b c".split(), attributes))

    def test__constructor__from_mapping(self, record_type):
        a, b, c = attributes = (-2 ** 31, 1234.5, 2 ** 15 - 1)
        mapping = {"a": a, "b": b, "c": c}

        source_bytes = struct.pack("ifH", *attributes)
        record = record_type.from_mapping(mapping)

        assert record["a"] == a
        assert record["b"] == b
        assert record["c"] == c

        assert len(record) == 3
        assert bytes(record) == source_bytes

        assert list(record.keys()) == "a b c".split()
        assert tuple(record.values()) == attributes
        assert list(record.items()) == list(zip("a b c".split(), attributes))

    def test__constructor__from_stream(self, record_type, tmpdir):
        a1, b1, c1 = attributes1 = (-2 ** 31, 1234.5, 2 ** 15 - 1)
        a2, b2, c2 = attributes2 = (2 ** 31 - 1, -1234.5, 800)

        source_bytes1 = struct.pack("ifH", *attributes1)
        source_bytes2 = struct.pack("ifH", *attributes2)
        source_path = tmpdir.join("record.bin")

        with open(str(source_path), "wb") as source_file:
            source_file.write(source_bytes1)
            source_file.write(source_bytes2)

        with open(str(source_path), "rb") as source_file:
            record1 = record_type.from_stream(source_file)
            record2 = record_type.from_stream(source_file)

        assert record1["a"] == a1 and record2["a"] == a2
        assert record1["b"] == b1 and record2["b"] == b2
        assert record1["c"] == c1 and record2["c"] == c2

        assert len(record1) == 3 and len(record2) == 3
        assert bytes(record1) == source_bytes1 and bytes(record2) == source_bytes2

        keys = "a b c".split()
        items1 = list(zip(keys, attributes1))
        items2 = list(zip(keys, attributes2))

        assert list(record1.keys()) == keys
        assert (
            tuple(record1.values()) == attributes1
            and tuple(record2.values()) == attributes2
        )
        assert list(record1.items()) == items1 and list(record2.items()) == items2

    def test__operation__equality(self, record_type):
        a, b, c = (-2 ** 31, 1234.5, 2 ** 15 - 1)

        record1 = record_type.from_mapping({"a": a, "b": b, "c": c})
        record2 = record_type.from_mapping({"a": a, "b": b, "c": c})

        assert record1 == record2

        record1["a"] = 9

        assert record1 != record2

    def test__operation__item_assignment(self, record_type):
        attributes1 = (-2 ** 31, 1234.5, 2 ** 15 - 1)
        a2, b2, c2 = attributes2 = (2 ** 31 - 1, -1234.5, 800)

        bytes1 = struct.pack("ifH", *attributes1)
        record = record_type.from_bytes(bytes1)

        bytes2 = struct.pack("ifH", *attributes2)

        record["a"] = a2
        record["b"] = b2
        record["c"] = c2

        assert record["a"] == a2
        assert record["b"] == b2
        assert record["c"] == c2

        assert len(record) == 3
        assert bytes(record) == bytes(bytes2)

        assert list(record.keys()) == "a b c".split()
        assert tuple(record.values()) == attributes2
        assert list(record.items()) == list(zip("a b c".split(), attributes2))

    def test__operation__repr(self, record_type):
        attributes1 = (-2 ** 31, 1234.5, 2 ** 15 - 1)
        attributes2 = (2 ** 31 - 1, -1234.5, 800)

        bytes1 = struct.pack("ifH", *attributes1)
        bytes2 = struct.pack("ifH", *attributes2)

        record1 = record_type.from_bytes(bytes1)
        record2 = record_type.from_bytes(bytes2)

        assert repr(record1) == "{}(a={!r}, b={!r}, c={!r})".format(
            record_type.__name__, *attributes1
        )
        assert repr(record2) == "{}(a={!r}, b={!r}, c={!r})".format(
            record_type.__name__, *attributes2
        )


class Test__Record__with_padding:
    @pytest.fixture(scope="class")
    def record_type(self):
        class RecordType(recparse.Record):
            FIELDS = [
                recparse.FieldSpec("a", "i"),
                recparse.FieldSpec("b", "f"),
                recparse.FieldSpec("c", "H"),
            ]
            PADDING = 2

        return RecordType

    def test__class_method__fields(self, record_type):
        fields = collections.OrderedDict(
            [
                ("a", recparse.FieldSpec("a", "i", offset=0)),
                ("b", recparse.FieldSpec("b", "f", offset=4)),
                ("c", recparse.FieldSpec("c", "H", offset=8)),
            ]
        )

        assert record_type.fields() == fields

    def test__class_method__padding(self, record_type):
        assert record_type.padding() == 2

    def test__class_method__struct_format(self, record_type):
        assert record_type.struct_format() == "ifH2x"

    def test__class_method__total_size(self, record_type):
        assert record_type.total_size() == 12

    def test__class_method__used_size(self, record_type):
        assert record_type.used_size() == 10

    def test__constructor__empty(self, record_type, tmpdir):
        a, b, c = attributes = struct.unpack("ifH", bytes(10))

        record = record_type.empty()

        assert record["a"] == a
        assert record["b"] == b
        assert record["c"] == c

        assert len(record) == 3
        assert bytes(record) == bytes(12)

        assert list(record.keys()) == "a b c".split()
        assert tuple(record.values()) == attributes
        assert list(record.items()) == list(zip("a b c".split(), attributes))

    @pytest.mark.parametrize("bytes_type", [bytes, bytearray])
    def test__constructor__from_bytes(self, record_type, bytes_type):
        a, b, c = attributes = (-2 ** 31, 1234.5, 2 ** 15 - 1)

        source_bytes = struct.pack("ifH", *attributes) + b"\x13\x17"
        record = record_type.from_bytes(bytes_type(source_bytes))

        assert record["a"] == a
        assert record["b"] == b
        assert record["c"] == c

        assert len(record) == 3
        assert bytes(record) == source_bytes

        assert list(record.keys()) == "a b c".split()
        assert tuple(record.values()) == attributes
        assert list(record.items()) == list(zip("a b c".split(), attributes))

    def test__constructor__from_iterable(self, record_type):
        a, b, c = attributes = (-2 ** 31, 1234.5, 2 ** 15 - 1)

        source_bytes = struct.pack("ifH", *attributes) + b"\x00\x00"
        record = record_type.from_iterable(attributes)

        assert record["a"] == a
        assert record["b"] == b
        assert record["c"] == c

        assert len(record) == 3
        assert bytes(record) == source_bytes

        assert list(record.keys()) == "a b c".split()
        assert tuple(record.values()) == attributes
        assert list(record.items()) == list(zip("a b c".split(), attributes))

    def test__constructor__from_mapping(self, record_type):
        a, b, c = attributes = (-2 ** 31, 1234.5, 2 ** 15 - 1)
        mapping = {"a": a, "b": b, "c": c}

        source_bytes = struct.pack("ifH", *attributes) + b"\x00\x00"
        record = record_type.from_mapping(mapping)

        assert record["a"] == a
        assert record["b"] == b
        assert record["c"] == c

        assert len(record) == 3
        assert bytes(record) == source_bytes

        assert list(record.keys()) == "a b c".split()
        assert tuple(record.values()) == attributes
        assert list(record.items()) == list(zip("a b c".split(), attributes))

    def test__from_stream__constructor(self, record_type, tmpdir):
        a1, b1, c1 = attributes1 = (-2 ** 31, 1234.5, 2 ** 15 - 1)
        a2, b2, c2 = attributes2 = (2 ** 31 - 1, -1234.5, 800)

        source_bytes1 = struct.pack("ifH", *attributes1) + b"\x13\x17"
        source_bytes2 = struct.pack("ifH", *attributes2) + b"\x14\x16"
        source_path = tmpdir.join("record.bin")

        with open(str(source_path), "wb") as source_file:
            source_file.write(source_bytes1)
            source_file.write(source_bytes2)

        with open(str(source_path), "rb") as source_file:
            record1 = record_type.from_stream(source_file)
            record2 = record_type.from_stream(source_file)

        assert record1["a"] == a1 and record2["a"] == a2
        assert record1["b"] == b1 and record2["b"] == b2
        assert record1["c"] == c1 and record2["c"] == c2

        assert len(record1) == 3 and len(record2) == 3
        assert bytes(record1) == source_bytes1 and bytes(record2) == source_bytes2

        keys = "a b c".split()
        items1 = list(zip(keys, attributes1))
        items2 = list(zip(keys, attributes2))

        assert list(record1.keys()) == keys
        assert (
            tuple(record1.values()) == attributes1
            and tuple(record2.values()) == attributes2
        )
        assert list(record1.items()) == items1 and list(record2.items()) == items2

    def test__operation__item_assignment(self, record_type):
        attributes1 = (-2 ** 31, 1234.5, 2 ** 15 - 1)
        a2, b2, c2 = attributes2 = (2 ** 31 - 1, -1234.5, 800)

        bytes1 = struct.pack("ifH", *attributes1) + b"\x13\x17"
        record = record_type.from_bytes(bytes1)

        bytes2 = struct.pack("ifH", *attributes2) + b"\x13\x17"

        record["a"] = a2
        record["b"] = b2
        record["c"] = c2

        assert record["a"] == a2
        assert record["b"] == b2
        assert record["c"] == c2

        assert len(record) == 3
        assert bytes(record) == bytes(bytes2)

        assert list(record.keys()) == "a b c".split()
        assert tuple(record.values()) == attributes2
        assert list(record.items()) == list(zip("a b c".split(), attributes2))


class Test__Record__with_size:
    @pytest.fixture(scope="class")
    def record_type(self):
        class RecordType(recparse.Record):
            FIELDS = [
                recparse.FieldSpec("a", "i"),
                recparse.FieldSpec("b", "f"),
                recparse.FieldSpec("c", "H"),
            ]
            SIZE = 14

        return RecordType

    def test__class_method__fields(self, record_type):
        fields = collections.OrderedDict(
            [
                ("a", recparse.FieldSpec("a", "i", offset=0)),
                ("b", recparse.FieldSpec("b", "f", offset=4)),
                ("c", recparse.FieldSpec("c", "H", offset=8)),
            ]
        )

        assert record_type.fields() == fields

    def test__class_method__padding(self, record_type):
        assert record_type.padding() == 4

    def test__class_method__struct_format(self, record_type):
        assert record_type.struct_format() == "ifH4x"

    def test__class_method__total_size(self, record_type):
        assert record_type.total_size() == 14

    def test__class_method__used_size(self, record_type):
        assert record_type.used_size() == 10

    def test__constructor__empty(self, record_type, tmpdir):
        a, b, c = attributes = struct.unpack("ifH4x", bytes(14))

        record = record_type.empty()

        assert record["a"] == a
        assert record["b"] == b
        assert record["c"] == c

        assert len(record) == 3
        assert bytes(record) == bytes(14)

        assert list(record.keys()) == "a b c".split()
        assert tuple(record.values()) == attributes
        assert list(record.items()) == list(zip("a b c".split(), attributes))

    @pytest.mark.parametrize("bytes_type", [bytes, bytearray])
    def test__constructor__from_bytes(self, record_type, bytes_type):
        a, b, c = attributes = (-2 ** 31, 1234.5, 2 ** 15 - 1)

        source_bytes = struct.pack("ifH4x", *attributes)
        record = record_type.from_bytes(bytes_type(source_bytes))

        assert record["a"] == a
        assert record["b"] == b
        assert record["c"] == c

        assert len(record) == 3
        assert bytes(record) == source_bytes

        assert list(record.keys()) == "a b c".split()
        assert tuple(record.values()) == attributes
        assert list(record.items()) == list(zip("a b c".split(), attributes))

    def test__constructor__from_iterable(self, record_type):
        a, b, c = attributes = (-2 ** 31, 1234.5, 2 ** 15 - 1)

        source_bytes = struct.pack("ifH4x", *attributes)
        record = record_type.from_iterable(attributes)

        assert record["a"] == a
        assert record["b"] == b
        assert record["c"] == c

        assert len(record) == 3
        assert bytes(record) == source_bytes

        assert list(record.keys()) == "a b c".split()
        assert tuple(record.values()) == attributes
        assert list(record.items()) == list(zip("a b c".split(), attributes))

    def test__constructor__from_mapping(self, record_type):
        a, b, c = attributes = (-2 ** 31, 1234.5, 2 ** 15 - 1)
        mapping = {"a": a, "b": b, "c": c}

        source_bytes = struct.pack("ifH4x", *attributes)
        record = record_type.from_mapping(mapping)

        assert record["a"] == a
        assert record["b"] == b
        assert record["c"] == c

        assert len(record) == 3
        assert bytes(record) == source_bytes

        assert list(record.keys()) == "a b c".split()
        assert tuple(record.values()) == attributes
        assert list(record.items()) == list(zip("a b c".split(), attributes))

    #

    def test__from_stream__constructor(self, record_type, tmpdir):
        a1, b1, c1 = attributes1 = (-2 ** 31, 1234.5, 2 ** 15 - 1)
        a2, b2, c2 = attributes2 = (2 ** 31 - 1, -1234.5, 800)

        source_bytes1 = struct.pack("ifH", *attributes1) + b"abcd"
        source_bytes2 = struct.pack("ifH", *attributes2) + b"1234"
        source_path = tmpdir.join("record.bin")

        with open(str(source_path), "wb") as source_file:
            source_file.write(source_bytes1)
            source_file.write(source_bytes2)

        with open(str(source_path), "rb") as source_file:
            record1 = record_type.from_stream(source_file)
            record2 = record_type.from_stream(source_file)

        assert record1["a"] == a1 and record2["a"] == a2
        assert record1["b"] == b1 and record2["b"] == b2
        assert record1["c"] == c1 and record2["c"] == c2

        assert len(record1) == 3 and len(record2) == 3
        assert bytes(record1) == source_bytes1 and bytes(record2) == source_bytes2

        keys = "a b c".split()
        items1 = list(zip(keys, attributes1))
        items2 = list(zip(keys, attributes2))

        assert list(record1.keys()) == keys
        assert (
            tuple(record1.values()) == attributes1
            and tuple(record2.values()) == attributes2
        )
        assert list(record1.items()) == items1 and list(record2.items()) == items2

    def test__operation__item_assignment(self, record_type):
        attributes1 = (-2 ** 31, 1234.5, 2 ** 15 - 1)
        a2, b2, c2 = attributes2 = (2 ** 31 - 1, -1234.5, 800)

        bytes1 = struct.pack("ifH", *attributes1) + b"1234"
        record = record_type.from_bytes(bytes1)

        bytes2 = struct.pack("ifH", *attributes2) + b"1234"

        record["a"] = a2
        record["b"] = b2
        record["c"] = c2

        assert record["a"] == a2
        assert record["b"] == b2
        assert record["c"] == c2

        assert len(record) == 3
        assert bytes(record) == bytes(bytes2)

        assert list(record.keys()) == "a b c".split()
        assert tuple(record.values()) == attributes2
        assert list(record.items()) == list(zip("a b c".split(), attributes2))
