========
Manifest
========

* `.bumpversion.cfg <https://gitlab.com/lcg/neuro/python-plx/blob/master/.bumpversion.cfg>`_ --- Configuration file for the bumpversion_ version-tagging package.
* `.coveragerc <https://gitlab.com/lcg/neuro/python-plx/blob/master/.coveragerc>`_ --- Configuration file for the Coverage_ reporting tool.
* `.gitignore <https://gitlab.com/lcg/neuro/python-plx/blob/master/.gitignore>`_ --- List of files and directories paths/patterns `ignored by Git <gitignore_>`_.
* `.gitlab-ci.yml <https://gitlab.com/lcg/neuro/python-plx/blob/master/.gitlab-ci.yml>`_ --- Continuous integration/deploy configuration (`GitLab CI`_), configured for:

  * Running a pytest_-based test suite and reporting results with Codecov_ at https://codecov.io/gl/lcg:neuro/python-plx,
  * Building the Sphinx_-based documentation and deploying it via `Gitlab Pages`_ to https://lcg.gitlab.io/neuro/python-plx, and
  * Uploading successfully built, tagged distributions to TestPyPI_ (defaults to https://test.pypi.org/project/lcg-neuro-plx).

* `.pre-commit-config.yaml <https://gitlab.com/lcg/neuro/python-plx/blob/master/.pre-commit-config.yaml>`_ --- Configuration file for the pre-commit_ package, which aids in applying useful `Git Hooks`_ in team workflows. Includes:

  * Automatic code styling with Black_.

* `CHANGELOG.rst <https://gitlab.com/lcg/neuro/python-plx/blob/master/CHANGELOG.rst>`_ --- History of changes.
* `CONTRIBUTING.rst <https://gitlab.com/lcg/neuro/python-plx/blob/0.1.0/CONTRIBUTING.rst>`_ --- General orientations for developers.
* `LICENSE.txt <https://gitlab.com/lcg/neuro/python-plx/blob/master/LICENSE.txt>`_ --- Copy of the `MIT License`_.
* `MANIFEST.rst <https://gitlab.com/lcg/neuro/python-plx/blob/master/MANIFEST.rst>`_ --- This very file, which details the repository's contents.
* `README.rst <https://gitlab.com/lcg/neuro/python-plx/blob/master/README.rst>`_ --- repository front-page.
* `docs/ <https://gitlab.com/lcg/neuro/python-plx/blob/master/docs>`_ --- Sphinx_-based documentation setup, which includes:

  * `conf.py <https://gitlab.com/lcg/neuro/python-plx/blob/master/docs/conf.py>`_ --- Sphinx_ configuration file,
  * `docs/index.rst <https://gitlab.com/lcg/neuro/python-plx/blob/master/docs/index.rst>`_ --- documentation master file, and
  * `Makefile <https://gitlab.com/lcg/neuro/python-plx/blob/master/docs/Makefile>`_ for building the docs easily.

* `poetry.lock <https://gitlab.com/lcg/neuro/python-plx/blob/master/poetry.lock>`_ --- Poetry_'s resolved dependency file. Whereas `pyproject.toml <https://gitlab.com/lcg/neuro/python-plx/blob/master/pyproject.toml>`_ specifies `as abstract as possible <setup-vs-requirements_>`__ dependencies, this file 
* `pyproject.toml <https://gitlab.com/lcg/neuro/python-plx/blob/master/pyproject.toml>`_ --- PEP-517_-compliant packaging metadata, configured with the Poetry_ system. Includes, among other information: package qualifiers, version, author, and all of its dependencies. This file replaces the classic setup.py_ file found in *classical* Python packaging.
* `src/plx <https://gitlab.com/lcg/neuro/python-plx/blob/master/plx>`_ --- Base directory of the example Python package distributed by this repository.
* `tests <https://gitlab.com/lcg/neuro/python-plx/blob/master/tests>`_ --- pytest_-powered test-suite.

.. _LICENSE: https://gitlab.com/lcg/neuro/python-plx/blob/master/LICENSE.txt
.. _MANIFEST: https://gitlab.com/lcg/neuro/python-plx/blob/master/MANIFEST.md
.. _README: https://gitlab.com/lcg/neuro/python-plx/blob/master/README.md
.. _Unreleased: https://gitlab.com/lcg/neuro/python-plx/compare?from=release&to=master
.. _0.1.0: https://gitlab.com/lcg/neuro/python-plx/tags/0.1.0
.. _MIT License: https://opensource.org/licenses/MIT

.. _Black: https://pypi.org/project/black/
.. _Codecov: https://codecov.io
.. _Coverage: https://coverage.readthedocs.io
.. _Git hooks: https://git-scm.com/book/en/v2/Customizing-Git-Git-Hooks
.. _Gitlab CI: https://docs.gitlab.com/ee/ci
.. _Gitlab Pages: https://docs.gitlab.com/ee/user/project/pages/
.. _Keep a Changelog: https://keepachangelog.com/en/1.0.0/
.. _PEP-517: https://www.python.org/dev/peps/pep-0517/
.. _Poetry: https://github.com/sdispater/poetry
.. _PyPI: https://pypi.org
.. _Semantic Versioning: https://semver.org/spec/v2.0.0.html
.. _Sphinx: https://www.sphinx-doc.org/en/master/index.html
.. _TestPyPI: https://test.pypi.org
.. _bumpversion: https://github.com/peritus/bumpversion
.. _gitignore: https://git-scm.com/docs/gitignore
.. _open-source: https://opensource.org/
.. _post-setup-vs-requirements: https://caremad.io/posts/2013/07/setup-vs-requirement/
.. _pre-commit: https://pre-commit.com/
.. _pytest: https://pytest.org/
.. _repository-codecov: https://codecov.io/gl/lcg:neuro/python-plx
.. _setup.py: https://docs.python.org/3.6/distutils/setupscript.html
.. _setup-vs-requirements: https://caremad.io/posts/2013/07/setup-vs-requirement/
