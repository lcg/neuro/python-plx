.. _plx_format:

=============================
Binary structure of PLX files
=============================

.. role:: cppcode(code)
   :language: c++

The PLX file is a Little Endian data format that records:

* A variety of raw and processed continuously-sampled (termed *continuous*, or :term:`AD`) electrical signals,
* Spikes, possibly sorted, and
* Controlling signals and experimental metadata, encoded in *event channels*.

Actual data is recorded at the final portion of the file, the `Data section`_, which follows the initial metadata sections, and is grouped in *channels*.
Each :term:`AD` channel corresponds to an electrode whose electrical signal is digitized by an :term:`ADC` card.
Event channels, on the other hand, are an abstraction for experimental metadata (usually by the software controlling the experiment, such as PsychToolbox3_) that is required for interpreting the results.

Various types of processing may be applied on raw :term:`AD` channels, such as low/high/band-pass filtering, and LFP_ extraction --- see documentation on the :class:`plx.parsing` module for details.
When spike detection is performed, waveform clips are recorded around the time in which the spike was detected, and spike sorting may optionally be performed during recording.
Spikes are recorded in *spike channels*, whereas other continuously sampled waveforms, whether raw or processed, may optionally be recorded in slow channels for future reference and re-processing --- in this case, file size may increase by a great deal.

The overall file structure is as follows:

.. csv-table::
    :header: Section,Size (bytes),Description

    `Main header`_,256,"File version, timestamp, lenght, number of channels, and overall channel and data resolution settings."
    `Counts`_,7248,"Auxiliary counters that may help in pre-allocating vectors for storing channel data in memory."
    `Spike headers <Spike channel header_>`_,"1020 times the number of spike channel headers (typically 96 in MAP_).",
    `Event headers <Event channel header_>`_,"296 times the number of event channel headers (typically 288 in MAP_).",
    `Slow headers <Slow channel header_>`_,"296 times the number of slow channel headers (typically 43 in MAP_).",
    `Data section`_,Variable,"An unordered sequence of spike, event, and slow channel samples that are each preceded by a `Data block header`_."

.. note::

    All text is encoded in ASCII format, stored with a :cppcode:`char []` C data-type.

.. note::

    The information on this pages is based on documents published in Plexon's `OmniPlex and MAP Offline DSK Bundle`_.

File sections
=============

Main header
-----------

.. csv-table::
    :header: Field,C type,Offset,Min. version,Description
    :widths: 20,10,10,10,50

    MagicNumber,:cppcode:`uint32_t`,0,,"Always equal to :cppcode:`0x58454c50`, which translates to :code:`PLEX`, in ASCII."
    Version,:cppcode:`int32_t`,4,,"Version of the data format; determines which data items are valid."
    Comment,:cppcode:`char [128]`,4,,"User-supplied comment."
    ADFrequency,:cppcode:`int32_t`,136,,"Timestamp (:term:`AD`) frequency, in hertz."
    NumDSPChannels,:cppcode:`int32_t`,140,,"Number of spike (:term:`DSP`) channel headers in the file."
    NumEventChannels,:cppcode:`int32_t`,144,,"Number of Event channel headers in the file."
    NumSlowChannels,:cppcode:`int32_t`,148,,"Number of slow (:term:`AD`) channel headers in the file."
    NumPointsWave,:cppcode:`int32_t`,152,,"Number of data points in waveform samples."
    NumPointsPreThr,:cppcode:`int32_t`,156,,"Number of data points in waveform samples that come before spike threshold-crossing time."
    Year,:cppcode:`int32_t`,160,,"This and following five fields indicate date/time when the data was acquired."
    Month,:cppcode:`int32_t`,164,,""
    Day,:cppcode:`int32_t`,168,,""
    Hour,:cppcode:`int32_t`,172,,""
    Minute,:cppcode:`int32_t`,176,,""
    Second,:cppcode:`int32_t`,180,,""
    FastRead,:cppcode:`int32_t`,184,,"Reserved."
    WaveformFreq,:cppcode:`int32_t`,188,,"Waveform sampling rate; ADFrequency above is timestamp frequency; both are usually equal."
    LastTimestamp,:cppcode:`float64_t`,192,,"Duration of the experimental session, in timestamp ticks."
    Trodalness,:cppcode:`int8_t`,200,103,"Electrode topology: 1 for single, 2 for stereotrode, 4 for tetrode."
    DataTrodalness,:cppcode:`int8_t`,201,103,"Trodalness of the data representation."
    BitsPerSpikeSample,:cppcode:`int8_t`,202,103,":term:`ADC` resolution for spike waveforms in bits (usually 12)."
    BitsPerSlowSample,:cppcode:`int8_t`,203,103,":term:`ADC` resolution for slow-channel data in bits (usually 12)."
    SpikeMaxMagnitudeMV,:cppcode:`uint16_t`,204,103,"The zero-to-peak voltage in milliVolts for spike waveform adc values (usually 3000)."
    SlowMaxMagnitudeMV,:cppcode:`uint16_t`,206,103,"The zero-to-peak voltage in milliVolts for slow-channel waveform adc values (usually 5000)."
    SpikePreAmpGain,:cppcode:`uint16_t`,208,105,"Usually either 1000 or 500."

The main header is followed by 46 padding bytes, so that it is 256 bytes long.

.. warning::

    The ``NumDSPChannels``, ``NumEventChannels``, and ``NumSlowChannels`` are not reliable counters.
    Neither are the counters in the Counts_ section, either because they do not cover all possible, or because they contain zeros when no spikes are recorded.
    Since extraneous spike/slow/event channel headers may also be present, the best way of estimating the number of actually recorded channels is simply exporting the data for each spike/slow/event channel separately, then try to guess the correct number of channels knowing what types of electrode matrices were used.

Counts
------

The first two arrays store counters for the number of timestamps and waveforms in each channel and unit.
Note that these only record the counts for the first 4 units in each channel.
Channel numbers are 1-based --- array entry 0 is unused.

The third array contains event counters --- roughly, the number of event data blocks per event channel.
Starting at index 300, this array also records the number of samples for the continuous channels.
Note that since it has only 512 entries, continuous channels above channel 211 do not have sample counts.

.. csv-table::
    :header: Field,C type,Offset,Description
    :widths: 20,10,10,60

    TSCounts,:cppcode:`int32_t [130][5]`,0,"Timestamps per :cppcode:`[channel][unit]`."
    WFCounts,:cppcode:`int32_t [130][5]`,2600,"Waveforms per :cppcode:`[channel][unit]`."
    EVCounts [1]_,:cppcode:`int32_t i [512]`,5200,"Event timestamps per :cppcode:`[event_channel]`."

This section is 2748 bytes long.

.. [1] Starting at index 300, this array also records the number of samples for the continuous channels.
       Note that since this field has only 512 entries, continuous channels above 211 do not have sample counts.

Channel headers
---------------

Spike channel header
~~~~~~~~~~~~~~~~~~~~

.. csv-table::
    :header: Field,C type,Offset,Description
    :widths: 20,10,10,60

    Name,:cppcode:`char [32]`,0,"Name given to the DSP channel."
    SIGName,:cppcode:`char [32]`,32,"Name given to the corresponding SIG channel."
    Channel,:cppcode:`int32_t`,64,":term:`DSP` channel number, 1-based."
    WFRate,:cppcode:`int32_t`,68,"When MAP_ is doing waveform rate limiting, this is limit waveform per second divided by 10."
    SIG,:cppcode:`int32_t`,72,"Signal channel associated with this DSP channel 1 - based."
    Ref,:cppcode:`int32_t`,76,"Signal channel used as a Reference signal, 1-based."
    Gain,:cppcode:`int32_t`,80,"Actual gain divided by SpikePreAmpGain. Prior to version 105, it is the actual gain divided by 1000."
    Filter,:cppcode:`int32_t`,84,"Either 0 or 1."
    Threshold,:cppcode:`int32_t`,88,"Threshold for spike detection in :term:`AD` values."
    Method,:cppcode:`int32_t`,92,"Method used for sorting units: 1 for boxes, 2 for templates."
    NUnits,:cppcode:`int32_t`,96,"Number of sorted units."
    Template,:cppcode:`int16_t [5][64]`,100,"Templates used for template sorting, in :term:`AD` values."
    "Fit",:cppcode:`int32_t [5]`,740,"Template fit."
    SortWidth,:cppcode:`int32_t`,760,"How many points to use in template sorting (template only)."
    Boxes,:cppcode:`int16_t [5][2][4]`,764,"The boxes used in boxes sorting (boxes only)."
    SortBeg,:cppcode:`int32_t`,844,"Beginning of the sorting window to use in template sorting (width defined by SortWidth)"
    Comment,:cppcode:`char [128]`,848,"User-supplied comment."

Each header is followed with 44 padding bytes, so that it is 1020 bytes long.


Event channel header
~~~~~~~~~~~~~~~~~~~~

Some software (such as PsychToolbox3_) may actually represent events by using event channel as *bits*.
For instance, channels 1-8 may correspond to an 8-bit code that corresponds to stimulus types;
hence, if an event is recorded on bit channels :math:`K` at time :math:`T`, it means that stimulus :math:`\sum_{k \in K} 2^{8-k-1}` was fired at time :math:`T`.

.. csv-table::
    :header: Field,C type,Offset,Description
    :widths: 20,10,10,60

    Name,:cppcode:`char [32]`,0,"Name given to this event."
    Channel,:cppcode:`int32_t`,32,"Event number, 1-based."
    Comment,:cppcode:`char [128]`,36,"User-supplied comment."

Each header is followed with 132 padding bytes, so that it is 296 bytes long.


Slow channel header
~~~~~~~~~~~~~~~~~~~

.. csv-table::
    :header: Field,C type,Offset,Description
    :widths: 20,10,10,60

    Name,:cppcode:`char [32]`,0,"Name given to this channel."
    Channel,:cppcode:`int32_t`,32,"Channel number, **0-based**."
    ADFreq,:cppcode:`int32_t`,36,"Digitization frequency, in hertz. It usually matches the :code:`ADFrequency` field in the main header."
    Gain,:cppcode:`int32_t`,40,"Gain at the :term:`ADC` card."
    Enabled,:cppcode:`int32_t`,44,"Whether this channel is enabled for taking data, 0 or 1. **Warning:** this field is not reliable, as it frequently contains 1 whereas no data was recorded for that channel."
    PreAmpGain,:cppcode:`int32_t`,48,"Gain at the pre-amplification phase."
    SpikeChannel,:cppcode:`int32_t`,52,"Spike channel (:code:`Channel` field of a `Spike channel header`_) of a spike channel corresponding to this continuous data channel. If 0, there is no associated spike channel."
    Comment,:cppcode:`char [128]`,56,"User-supplied comment."

Each header is followed with 112 padding bytes, so that it is 296 bytes long.


Data section
------------

Data block header
~~~~~~~~~~~~~~~~~

.. csv-table::
    :header: Field,C type,Offset,Description
    :widths: 20,10,10,60

    Type,:cppcode:`int16_t`,0,"Data type: 1 for spike, 4 for event, and 5 for continuous."
    TimestampUpper,:cppcode:`uint16_t`,2,"Upper 8 bits of the 5-byte timestamp."
    TimestampLower,:cppcode:`uint32_t`,4,"Lower 32 bits of the 5-byte timestamp."
    Channel,:cppcode:`int16_t`,8,"Channel number."
    Unit,:cppcode:`int16_t`,10,"Sorted unit number; 0 means unsorted."
    NoWf,:cppcode:`int16_t`,12,"Number of waveforms in the data to follow, usually 0 or 1."
    NoWoWf,:cppcode:`int16_t`,14,"Number of samples (2-byte words) per waveform in the data to follow."

Data block headers have no padding (they are just 16 bytes long).
The timestamp can be converted to an 8-byte unsigned integer timestamp with the following C++ expression:

.. code:: c++

    static_cast<uint64_t>(TimestampUpper)<<32 + static_cast<uint64_t>(TimestampLower)

Spike and slow data block headers area usually followed by waveform samples, whereas event data block headers are immediately followed by another header.
Spike data block headers usually have :cppcode:`WoWf == 1` and :cppcode:`NoWoWf` equal to the :code:`NumPointsWave` field in the `Main header`_.
Slow data block headers usually have :cppcode:`WoWf == 1` and :cppcode:`NoWoWf` equal to a greater number (such as 40 or 120) of digitized waveform samples that follow.
Event data block headers should have both :cppcode:`WoWf == 0` and :cppcode:`NoWoWf == 0`.
In general, the number of bytes that should be skipped in order to read the next data block header is given by

.. code:: c++

    sizeof(int16_t) * NoWf * NoWoWf

See the following sections on how to interpret spike/slow data.


Spike data blocks
~~~~~~~~~~~~~~~~~

For PLX version prior to 103:

.. math::

    {\rm voltage} = \frac{3000 \times {\rm sample}}{2048 \times 1000 \times {\rm spkheader.Gain}}

For versions from 103 and prior to 105:

.. math::

    {\rm voltage} = \frac{{\rm header.SpikeMaxMagnitudeMV} \times {\rm sample}}{2^{{\rm header.BitsPerSpikeSample}-1} \times 1000 \times {\rm spkheader.Gain}}

And for versions from 105:

.. math::

    {\rm voltage} = \frac{{\rm header.SpikeMaxMagnitudeMV} \times {\rm sample}}{2^{{\rm header.BitsPerSpikeSample}-1} \times {\rm header.SpikePreAmpGain} \times {\rm spkheader.Gain}}

.. note::

    Spike pre-amplification gain is a global setting stored in the :code:`SpikePreAmpGain` gain of the `Main header`_.

.. todo::

    Add normalization methods to the API.
    Currently, only accessing the :term:`ADC` values is possible.

Slow data blocks
~~~~~~~~~~~~~~~~

For PLX version prior to 102:

.. math::

    {\rm voltage} = \frac{5000 \times {\rm sample}}{2048 \times 1000 \times {\rm slowheader.Gain}}

For PLX version 102:

.. math::

    {\rm voltage} = \frac{5000 \times {\rm sample}}{2048 \times {\rm slowheader.PreAmpGain} \times {\rm slowheader.Gain}}

For PLX version from 103:

.. math::

    {\rm voltage} = \frac{{\rm header.SlowMaxMagnitudeMV} \times {\rm sample}}{2^{{\rm header.BitsPerSlowSample} - 1} \times {\rm slowheader.PreAmpGain} \times {\rm slowheader.Gain}}

.. note::

    Slow waveform pre-amplification gain is a per-channel setting stored in the :code:`PreAmpGain` gain of each corresponding `Slow channel header`_.

.. todo::

    Add normalization methods to the API.
    Currently, only accessing the :term:`ADC` values is possible.

.. _LFP: https://en.wikipedia.org/wiki/Local_field_potential
.. _MAP: https://plexon.com/products/map-data-acquisition-system-plexon/
.. _OmniPlex and MAP Offline DSK Bundle: https://plexon.com/wp-content/uploads/2017/08/OmniPlex-and-MAP-Offline-SDK-Bundle_0.zip
.. _PsychToolbox3: http://psychtoolbox.org/
