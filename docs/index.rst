==============================================================
plx --- Parsing of PLX neurophysiological data files in Python
==============================================================

.. toctree::
   :maxdepth: 1

   Project information <README>
   quickref
   plx
   cli
   api/index
   glossary
   CONTRIBUTING
   MANIFEST
   CHANGELOG


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

