.. _Attrs: https://www.attrs.org/en/stable/
.. _LCG Neuroscience folder: https://drive.google.com/open?id=1UieuXEmeroAteDgu7VvN1oJW7mc5o2YB
.. _LFC: https://www.biof.ufrj.br/pt-br/node/44
.. _Marshmallow: https://marshmallow.readthedocs.io/en/latest
.. _Marshmallow-annotations: https://marshmallow-annotations.readthedocs.io/en/latest
.. _Pint: https://pint.readthedocs.io/en/latest/
.. _ProjectV2 dataset: https://drive.google.com/open?id=1BhOSRubJUuMoFyTYzM-GKC7ChYOmVRrj
.. _ProjectV2 repository: https://gitlab.com/lcg/v2/projectv2
.. _SQLAlchemy declarative base: https://docs.sqlalchemy.org/en/latest/orm/extensions/declarative/index.html
.. _SQLAlchemy declarative mixins: https://docs.sqlalchemy.org/en/latest/orm/extensions/declarative/mixins.html
.. _SQLAlchemy declarative: https://docs.sqlalchemy.org/en/latest/orm/extensions/declarative/index.html
.. _SQLAlchemy engine: https://docs.sqlalchemy.org/en/latest/core/engines.html
.. _SQLAlchemy joined-table inheritance: https://docs.sqlalchemy.org/en/latest/orm/inheritance.html#joined-table-inheritance
.. _SQLAlchemy scoped session: https://docs.sqlalchemy.org/en/latest/orm/contextual.html
.. _SQLAlchemy scoped sessions: https://docs.sqlalchemy.org/en/latest/orm/contextual.html
.. _SQLAlchemy session: https://docs.sqlalchemy.org/en/latest/orm/session.html
.. _SQLAlchemy tutorial: https://docs.sqlalchemy.org/en/latest/orm/tutorial.html
.. _SQLAlchemy: https://www.sqlalchemy.org/
.. _SQLite: https://www.sqlite.org/index.html
.. _declarative base: https://docs.sqlalchemy.org/en/latest/orm/extensions/declarative/index.html
.. _environ_config: https://github.com/hynek/environ_config
.. _lcg: http://www.lcg.ufrj.br
.. _neuroscience projects: https://lcg.gitlab.io/neuroscience
.. _projectv2-viz-site: http://lcg.ufrj.br/projectv2-viz
.. _projectv2-viz: https://lcg.gitlab.io/projectv2-viz
.. _python-plx: https://gitlab.com/lcg/neuro/python-plx
.. _rclone: https://rclone.org/
.. _scoped session: https://docs.sqlalchemy.org/en/latest/orm/contextual.html
.. _scoped sessions: https://docs.sqlalchemy.org/en/latest/orm/contextual.html
.. _ufrj: https://www.ufrj.br
