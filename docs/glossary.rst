========
Glossary
========

.. glossary::

    AD
        Analog-to-digital.

    ADC
        Analog-to-digital converter.

    DSP
        Digital-signal-processing.
