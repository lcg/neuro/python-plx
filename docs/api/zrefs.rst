.. vim: et: sw=3: ts=3: tw=120:
   vim: filetype=rst:
   vim: spell: spelllang=en:
   vim: foldmethod=marker:

============
Bibliography
============

.. bibliography:: zrefs.bib
