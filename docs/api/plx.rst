.. vim: et: sw=3: ts=3: tw=120:
   vim: filetype=rst:
   vim: spell: spelllang=en:
   vim: foldmethod=marker:

=================================================
High-level access to raw and exported *PLX* files
=================================================

:mod:`plx` -- Core functionality
================================

.. automodule:: plx
   :members:
   :show-inheritance:
   :undoc-members:

:mod:`plx.schemas` -- Marshmallow schemas for JSON exporting
============================================================

.. automodule:: plx.schemas
   :members:
   :show-inheritance:
   :undoc-members:
