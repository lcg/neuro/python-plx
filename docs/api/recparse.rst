.. vim: et: sw=3: ts=3: tw=120:
   vim: filetype=rst:
   vim: spell: spelllang=en:
   vim: foldmethod=marker:

==================================================
:mod:`recparse` -- Declarative binary file parsing
==================================================

.. automodule:: recparse
   :members:
   :show-inheritance:
   :undoc-members:

