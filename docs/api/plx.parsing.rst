.. vim: et: sw=3: ts=3: tw=120:
   vim: filetype=rst:
   vim: spell: spelllang=en:
   vim: foldmethod=marker:

==========================================
:mod:`plx.parsing` -- *PLX* file structure
==========================================

.. automodule:: plx.parsing
   :members:
   :show-inheritance:
   :undoc-members:
