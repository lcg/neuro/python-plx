.. vim: et: sw=3: ts=3: tw=120:
   vim: filetype=rst:
   vim: spell: spelllang=en:
   vim: foldmethod=marker:

=======================================================================
:mod:`_plx` -- C-extension module for faster *PLX* channel data exports
=======================================================================

.. automodule:: _plx
   :members:
   :show-inheritance:
   :undoc-members:
