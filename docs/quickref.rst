===============
Quick-reference
===============

This page shows some quick recipes for working with the lcg-neuro-plx_ library.
The recipes usually assume a hypothetical scenario in which you have either

* a PLX file located at ``/path/to/file.plx``, or
* exported PLX file [1]_ parts located at ``/path/to/export``.

.. contents:: Recipes
    :local:

.. [1] This may done using either the :ref:`Command-line interface <cli>`, or the :meth:`plx.PlxFile.export_file` method.
       In both cases, if the file is located at ``/path/to/file.plx``, the default export tree will be at ``/path/to/export``.


Installation
============

Make sure you have a C++ compiler installed (like ``clang``, or ``g++``), *e.g.*:

.. code:: bash

    sudo apt install -y g++

Then, install lcg-neuro-plx_ with ``pip`` into your local development environment, *e.g.*:

.. code:: bash

    # If installing in a virtualenv (recommended), do (after activating the virtualenv)
    pip install lcg-neuro-plx

    # If installing system-wise (not recommended), do
    sudo pip install lcg-neuro-plx

    # If installing as a user package (if you insist on not using virtualenvs), do
    pip install --user lcg-neuro-plx

.. _recipe_opening_a_plx_file:

Opening a PLX file
==================

If the file has been previously exported to binary format, you can open it with

.. code:: python

    import plx

    plx_file = plx.ExportedPlxFile('path/to/exported')

Otherwise, you may open a PLX file directly with

.. code:: python

    import plx

    plx_file = plx.PlxFile('path/to/file.plx')

In either case, the ``plx_file`` object will have the basic properties declared in the :class:`plx.AbstractPlxFile` interface.
Take a look at this class' documentation, or at the :ref:`plx_format` section to understand what you'll find inside a PLX file.

Opening PLX file sections
=========================

.. todo::
    Write on how to open :class:`counts section <plx.parsing.Counts>` and :class:`event data <plx.parsing.Counts>`.

.. _recipe_opening_plx_main_file_header:

Main file header
----------------

If you followed :ref:`recipe_opening_a_plx_file`, you may simply do

.. code:: python

    plx_file.header

Otherwise, if you know the path to the exported header, do

.. code:: python

    import plx.parsing

    plx_header = plx.parsing.Header.from_file('/path/to/export/header.bin')

Finally, if you want to open it as a Numpy array, you can do

.. code:: python

    import numpy
    import plx.parsing

    plx_header = numpy.fromfile('/path/to/export/header.bin',
                                dtype=plx.parsing.Header.dtype)

in which case ``plx_header`` will be a record array of shape ``(1,)`` with a structured data-type that describes all fields normally found in a :class:`PLX main file header <plx.parsing.Header>`.


.. _plx_opening_spike_data:
.. _plx_opening_spike_headers:

Spike headers and data
----------------------

If you followed :ref:`recipe_opening_a_plx_file`, you may simply do

.. code:: python

    spk_chan = plx_file.spike_channel(1)  # The argument is the 1-based number of the desired spike channel.

in which case the returned object will be of type :class:`plx.SpikeChannel`, and you can use the :attr:`header <plx.SpikeChannel.header>` (type :class:`plx.parsing.SpikeHeader`) and :attr:`data <plx.SpikeChannel.data>` (a Numpy array) to get access to the header information and the raw recorded spike data, respectively.
Spike headers may be also accessed separately with

.. code:: python

    spk_header = plx_file.spike_headers[0]  # The index is 0-based, hence one less than the label number of the desired spike channel.

But in order to read spike data directly from exported binary data, you'll need to figure out the correct Numpy data-type, as it depends on the number of waveform samples that were recorded [2]_ in the original PLX file (:attr:`plx.parsing.Header.num_points_wave` property).
You can extract that information and build the Numpy data-type in a couple of different ways:

* From the :ref:`exported file header <recipe_opening_plx_main_file_header>`

  .. code:: python

      import numpy
      import plx.export

      dtype = plx.export.ExportedSpike(num_points_wave=plx_header.num_points_wave).dtype
      spk_data = numpy.fromfile('/path/to/export/spike_data/1.bin', dtype=dtype)

* From the :ref:`original/exported file object <recipe_opening_a_plx_file>`

  .. code:: python

      import numpy
      import plx.export

      dtype = plx.export.ExportedSpike(plx_file=plx_file).dtype
      spk_data = numpy.fromfile('/path/to/export/spike_data/1.bin', dtype=dtype)

Finally, you may parse exported spike headers into an Numpy array with

.. code:: python

    import numpy
    import plx.parsing

    spk_headers = numpy.fromfile('/path/to/export/spike_headers.bin', dtype=plx.parsing.SpikeHeader.dtype)
    spk_header_1 = spk_headers[0]  # Notice that it is a Python list, so index is 0-based.

.. [2] That number is usually 32, so you can give it a try.


.. _plx_opening_slow_data:
.. _plx_opening_slow_headers:

Continuous headers and data
---------------------------

If you followed :ref:`recipe_opening_a_plx_file`, you may simply do

.. code:: python

    slow_chan = plx_file.slow_channel(1)  # The argument is the 1-based number of the desired slow channel.

in which case the returned object will be of type :class:`plx.SlowChannel`, and you can use the :attr:`header <plx.SlowChannel.header>` (type :class:`plx.parsing.SlowHeader`) and :attr:`data <plx.SlowChannel.data>` (a Numpy array) to get access to the header information and the raw recorded slow data, respectively.
Slow headers may be also accessed separately with

.. code:: python

    slow_header = plx_file.slow_headers[0]  # The index is 0-based.

In order to read slow data directly from exported binary data, you can use the Numpy data type declared in :data:`plx.export.exported_slow_dtype`, which is just a placeholder for ``int16`` (signed 16-bit integers), after all:

.. code:: python

    import numpy
    import plx.export

    dtype = plx.export.exported_slow_dtype
    slow_data = numpy.fromfile('/path/to/export/slow_data/1.bin', dtype=dtype)

Finally, you may parse exported slow headers into an Numpy array with

.. code:: python

    import numpy
    import plx.parsing

    slow_headers = numpy.fromfile('/path/to/export/slow_headers.bin', dtype=plx.parsing.SlowHeader.dtype)
    slow_header_0 = slow_headers[0]

.. _lcg-neuro-plx: https://pypi.org/project/lcg-neuro-plx
