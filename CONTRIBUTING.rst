==================
Contribution guide
==================

Development setup
=================

**Requirements:**

* Python 3.6
* Poetry 0.12

1. Create a virtualenv_ and install dependencies. Two of the most common options are:

    * Letting Poetry_ create the virtual environment and install dependencies for you (the default if you have just installed Poetry):

      .. code:: bash

          poetry install  # This will create a virtual environment in a new subdirectory
                          # of $(poetry config settings.virtualenvs.path)

      An interesting variation is telling Poetry to create the environment inside the project's root folder by running

      .. code:: bash

          poetry config settings.virtualenvs.in-project true

      before the ``install`` command.

    * If you have chosen to turn off automatic environment creation using

      .. code:: bash

          poetry config settings.virtualenvs.create false

      then the following steps should suffice:

      .. code:: bash

          virtualenv -p $(which python3.6) .venv
          source venv/bin/activate.sh
          poetry install

      In this case, keep in mind that you need to activate the environment using the ``source`` command before issuing any Poetry commands.
      You may choose a name for your virtual environment other than `.venv`, but this one is already on `.gitignore <https://gitlab.com/lcg/neuro/python-plx/blob/master/.gitignore>`_.

1. **(Optional)** Set up pre-commit_. It is already included as *dev*-dependency in `pyproject.toml <https://gitlab.com/lcg/neuro/python-plx/blob/master/pyproject.toml>`_ and a `.pre-commit-config.yaml <https://gitlab.com/lcg/neuro/python-plx/blob/master/.pre-commit-config.yaml>`_ file is provided.
   Hence, you just need:

   .. code:: bash

       pre-commit install


Building the documentation
==========================

Provided that the virtual environment is active, and dependencies have been installed, it is as simple as

.. code:: bash

    make html

and the output will be placed under ``docs/_build/html``.

The `CI/CD <Gitlab CI_>`_ pipeline, through `GitLab Pages`_, will automatically deploy the HTML tree to https://lcg.gitlab.io/neuro/python-plx.
If you clone the repository and do not make further changes, documentation built after ``git push`` will most likely end up in https://organization-name.gitlab.io/group-name-if-any/repository-name.


Running the tests
=================

Provided that the virtual environment is active, and dependencies have been installed, it is as simple as

.. code:: bash

    pytest

This already produces a code coverage report for the `app <https://gitlab.com/lcg/neuro/python-plx/blob/master/app>`_ package, stores it in a ``.coverage`` file and prints it.


Publishing on PyPI or TestPyPI
==============================

You can easily publish your package to PyPI_ or TestPyPI_ using ``poetry publish``, or Twine_.
For instance (assuming you already have registered for an account on TestPyPI_), you can

.. code:: bash

    poetry config repositories.pypi https://upload.pypi.org/legacy
    poetry config http-basic.pypi <PYPI_USERNAME> <PYPI_PASSWORD>

just once; then, for every version you want to publish, run

.. code:: bash

    poetry build && poetry publish -r pypi # or
    poetry publish --build -r pypi         # or

Alternatively, if you want to use Twine_, you can install it with ``pip install twine``, then

.. code:: bash

    poetry build && twine upload --repository-url https://upload.pypi.org/legacy dist/*

    # If using the default pypi repository, this form also works
    poetry build && twine upload dist/*

which can also be made more usable by creating a ``.pypirc`` file.
After running `poetry build`, you can check the distribution files are ok by running ``tar tvzf dist/*.tgz`` and inspecting the output.
If you do not configure Poetry_ for TestPyPI_ or another repository using commands like the ones above, it will publish, by default, to PyPI_.

If you fork this repository, set up CI variables for ``PYPI_USERNAME`` and ``PYPI_PASSWORD``, or hard-code them (not recommended, for security reasons) directly in the `.gitlab-ci.yml <https://gitlab.com/lcg/neuro/python-plx/blob/master/.gitlab-ci.yml>`_ script.

.. _Gitlab CI: https://docs.gitlab.com/ee/ci
.. _Gitlab Pages: https://docs.gitlab.com/ee/user/project/pages/
.. _Poetry: https://github.com/sdispater/poetry
.. _PyPI: https://pypi.org
.. _TestPyPI: https://test.pypi.org
.. _Twine: https://twine.readthedocs.io
.. _pre-commit: https://pre-commit.com/
.. _pytest: https://pytest.org/
.. _virtualenv: https://virtualenv.pypa.io/en/latest/
